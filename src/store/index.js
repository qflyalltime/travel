import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    city: localStorage.city || '北京',
    showGallery: false
  },
  mutations: {
    changeCity (state, city) {
      state.city = city
    },
    changeShowGallery (state) {
      state.showGallery = !state.showGallery
    }
  }
})
